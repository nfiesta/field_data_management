#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
import sys
import psycopg2
import psycopg2.extras
from psycopg2 import sql
import zipfile
import xml.etree.ElementTree as ET

def check_file(fname):
    print('check file %s ' % fname)
    try:
        with zipfile.ZipFile(fname, 'r') as zip_ref:
            data = list()
            for xml_fn in zip_ref.namelist():
                if xml_fn.endswith('.xml'):
                    f = zip_ref.open(xml_fn)
                    content = f.read()
                    try:
                        root = ET.fromstring(content)
                        '''for child in root:
                            print(child.tag, child.attrib)'''
                        ET.indent(root)
                        data.append({'fname': xml_fn, 'xmlvalue': ET.tostring(root, encoding='unicode')})
                    except ET.ParseError as e:
                        print("xml_loader: not possible to parse XML")
                        print(e)
                        sys.exit(1)
            return data
    except zipfile.BadZipFile as e:
        print("xml_loader: not possible to read ZIP archive")
        print(e)
        sys.exit(1)
    except FileNotFoundError as e:
        print("xml_loader: File not found")
        print(e)
        sys.exit(1)

def push_file(fname, connstr):
    content = check_file(fname)
    print('push with connstr %s' % (connstr))

    try:
        # alter database contrib_regression_user rename to nfi_analytical;
        conn = psycopg2.connect(connstr)
    except psycopg2.DatabaseError as e:
        print("xml_loader: not possible to connect DB")
        print(e)
        sys.exit(1)

    cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

    sql_cmd_import = '''insert into field_data_management.xml_import (zip_filename, filename, xml_value) 
                        values (%(filename)s, %(fname)s, %(xmlvalue)s)
                        returning id;'''
    content[0].update({'filename':fname})
    
    try:
        cur.execute(sql_cmd_import, content[0])
    except psycopg2.Error as e:
        print("xml_loader: SQL error:")
        print(e)
        sys.exit(1)

    if cur.description != None:
        path_row_list = cur.fetchall()
    else:
        path_row_list = None
    
    import_id = path_row_list[0][0]
    
    sql_cmd_data = '''insert into field_data_management.xml_data (import, filename, xml_value) values (%(import_id)s, %(fname)s, %(xmlvalue)s);'''

    for d in content[1:]:
        d.update({'import_id': import_id})
        try:
            cur.execute(sql_cmd_data, d)
        except psycopg2.Error as e:
            print("xml_loader: SQL error:")
            print(e)
            sys.exit(1)

    sql_plots = '''
SELECT
        import, id,
        xmltable.*
FROM field_data_management.xml_data
, XMLTABLE('//plots'
        PASSING xml_value
        COLUMNS
                plot_id text PATH 'plot_id/code',
                team text PATH 'team/value',
                survey_date text PATH 'concat(survey_date/month, ".", survey_date/month, ".", survey_date/year)')
;'''
    
    sql_imports = '''select
        xml_import.id, xml_import.zip_filename, xml_import.uploaded,
        xml_data.filename, xpath('//plot_id/code/text()', xml_data.xml_value) as plot_id
from field_data_management.xml_import
join field_data_management.xml_data on (xml_import.id = xml_data.import);'''

    sql_trees = '''
SELECT filename, (xpath('plots/plot_id/code/text()', xml_value))[1] as plot_id,
        xmltable.*
FROM field_data_management.xml_data
, XMLTABLE('/plots/tree'
        PASSING xml_value
        COLUMNS
                plot_id text PATH '../plot_id/code',
                tree_id text PATH 'tree_id/value',
                tree_species text PATH 'tree_species/scientific_name',
                tree_dbh text PATH 'tree_dbh/value');'''

    try:
        cur.execute(sql_trees)
    except psycopg2.Error as e:
        print("xml_loader: SQL error:")
        print(e)
        sys.exit(1)

    if cur.description != None:
        path_row_list = cur.fetchall()
    else:
        path_row_list = None
    
    for row in path_row_list:
        print(row)

    cur.close()
    conn.commit()
    conn.close()

def main():
    parser = argparse.ArgumentParser(description="XML data loader",
            formatter_class=argparse.RawDescriptionHelpFormatter,
            epilog='''\
Example:
    * check XML
        python3 xml_loader.py -i=datasource.zip
    * changescript -> changescript (update of funcions in changescript)
        python3 xml_loader.py -i=datasource.zip -c='dbname=MyDB host=CanBeOmmitedForLinuxBasedAuthentication user=MyUser'
    for password use ~/.pgpass file
            ''')
    parser.add_argument('-c', '--connstr', required=False, help='PostgreSQL connection string (https://www.postgresql.org/docs/current/libpq-connect.html#LIBPQ-CONNSTRING)')
    parser.add_argument('-i', '--infile', required=True, help='input file to scan')

    args = parser.parse_args()

    if args.connstr:
        check_file(args.infile)
        push_file(args.infile, args.connstr)
    else:
        check_file(args.infile)

if __name__ == '__main__':
    main()
